import {combineReducers} from 'redux-immutable'
import {reducer as playReducer} from '../components/play/store/index'
// import {reducer as loginReducer} from '../pages/login/store/index'
// import {reducer as userReducer} from '../pages/users/store/index'
// import {reducer as ordersReducer} from '../pages/orders/store/index'
// import {reducer as authReducer} from '../pages/auth/store/index'
const reducer = combineReducers({
  play:playReducer,
  // login:loginReducer,
  // users:userReducer,
  // orders:ordersReducer,
  // auth:authReducer,
})
export default reducer