import React, { Component } from 'react'

// 导入路由
import Router from './router/index'
//导入播放主页
import Play from './components/play/Play'
//导入播放底部
import Bottom from './components/bottom/Bottom'
import './utils/rem'
// 定义组件
class App extends Component{
 render() {
     return (
         <div style={{width:"100%",height:"100%",overflow:"hidden"}}>
             <Router />
            
             <Play/>
             <Bottom />
         </div>
     )
 }
}

export default App
