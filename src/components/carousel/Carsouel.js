import React, { Component } from 'react'
//引入swiper相关
 import Swiper from "swiper"
 import "swiper/css/swiper.css"
//导入样式组件
import {CarsouelStyle} from './CarsouelStyle'
export default class Carsouel extends Component {
  
  componentDidMount(){
      new Swiper ('.swiper-container', {
        // direction: 'vertical', // 垂直切换选项
        loop: true, // 循环模式选项
        autoplay:true,//自动切换
        // 如果需要分页器
        pagination: {
          el: '.swiper-pagination',
        },
        // // 如果需要滚动条
        // scrollbar: {
        //   el: '.swiper-scrollbar',
        // },
      })              
  } 
  render() {
    return (
      <CarsouelStyle>
        <div className="swiper-container">
          <div className="swiper-wrapper">
              <div className="swiper-slide">Slide 1</div>
              <div className="swiper-slide">Slide 2</div>
              <div className="swiper-slide">Slide 3</div>
          </div>
          {/* <!-- 如果需要分页器 --> */}
          <div className="swiper-pagination"></div>
          {/* <!-- 如果需要滚动条 --> */}
          <div className="swiper-scrollbar"></div>
      </div>
      </CarsouelStyle>
    )
  }
}
