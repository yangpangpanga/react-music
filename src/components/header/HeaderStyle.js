import styled from 'styled-components'
export const HeaderStyle = styled.div`
width:100%;
height:.44rem;
display:flex;
justify-content:center;
align-items:center;
font-size:.2rem;
  img{
    width:.3rem;
    height:.3rem;
    margin:0 .1rem;
  }
`