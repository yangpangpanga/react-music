import React, { Component } from 'react'
//导入样式组件
import {HeaderStyle} from './HeaderStyle'
export default class Head extends Component {
  render() {
    return (
      <HeaderStyle>
        <img src="/logo192.png" alt="音乐"/>
        <p>music</p>
      </HeaderStyle>
    )
  }
}
