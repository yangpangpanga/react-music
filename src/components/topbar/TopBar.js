import React, { Component } from 'react'
import {NavLink} from 'react-router-dom'
//导入组件样式

import{TopBarStyle} from './TopBarStyle'
export default class TopBar extends Component {
  render() {
    return (
      <TopBarStyle>
        <NavLink to="/recommend">推荐</NavLink>
        <NavLink to="/singer">歌手</NavLink>
        <NavLink to="/rank">排行</NavLink>
        <NavLink to="/search">搜索</NavLink>
      </TopBarStyle>
    )
  }
}
