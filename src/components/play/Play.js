import React, { Component } from 'react'
import {connect} from 'react-redux'
//antd
import { Toast } from 'antd-mobile';
//导入action
import {playing,playingsmall,setUrl,setCount} from '../play/store/actionsCreator'
//导入请求
import {getSongUrl} from '../../api/requestw'
//导入antd组件
import { Slider } from 'antd-mobile';
//导入样式组件
import {PlayStyle} from './PlayStyle'
import {ControlStyle} from './PlayStyle'
 class Play extends Component {
  constructor(props){
    super(props)
  }
  changeFn(val){
    console.log(val)
  }
  AfterChangeFn(val){
    console.log(val)
  }
  //控制大播放器隐藏小播放器显示
  showSmall(val){
    this.props.changeBigSize(!val)
    this.props.changeSmallSize(val)
  }
  //开始播放
  startPlay(){
    console.log('我要开始播放了',this.props.count)
    if(this.props.playSrc){
      this.refs.listen.play()
    }else{
      Toast.info('该歌曲需要vip奥~~~~', 1);
    }
    
  }
  pausePlay(){
    this.refs.listen.pause()
  }
  nextPlay(){
    let num = this.props.count + 1
    console.log(num)
    this.props.playCount(num) //设置播放的序号
    const id = this.props.willPlayList[num].id
    getSongUrl({id}).then(res => {
      this.props.playUrl(res.data[0].url)  //设置播放的url
    })
    // this.props.playUrl(this.props.willPlayList[num].purl)
  }
  render() {
    if(this.props.count >= 0){
    return (
      <ControlStyle style={ this.props.status? {'display':'block'}:{'display':'none'}}>
      <PlayStyle img="https://y.gtimg.cn/music/photo_new/T002R300x300M000000nQcZb3y74Hb.jpg?max_age=2592000">
        <div className="content">
          <div className="title">
          <i onClick={this.showSmall.bind(this,true)}>图标</i><span>歌曲名字,歌曲名字</span>
          </div>
          <h2>歌手名字</h2>
          <div className="center">
            <img src="https://y.gtimg.cn/music/photo_new/T002R300x300M000000dUlqW3lbyob.jpg?max_age=2592000" alt="" />
            <p>这里面是一句歌词啊</p> 
          </div>
          <audio ref="listen" src={this.props.playSrc} style={{display:'none'}}></audio>
          {/* 滑块控制区域 */}
          <div className="control">
            <span>0.00</span>
            <Slider
              style={{ marginLeft: 30, marginRight: 30 }}
              defaultValue={26}
              min={0}
              max={30}
              onChange={this.changeFn.bind(this,'change')}
              onAfterChange={this.AfterChangeFn.bind(this,'afterChange')}
            />
            <span>6.66</span>
          </div>
          {/* 按钮区域 */}
          <div className="btn">
            <span>1</span>
            <span>2</span>
            <span onClick={this.startPlay.bind(this)}>3</span>
            <span onClick={this.pausePlay.bind(this)}>4</span>
            <span onClick={this.nextPlay.bind(this)}>5</span>
          </div>
        </div>
      </PlayStyle>
      </ControlStyle>
    )
    }else{
      return <div></div>
    }
    
  }
}
const mapStateToProps = state => {
  return{
    status:state.toJS().play.status ,//是否播放
    willPlayList:state.toJS().play.playList,//播放列表信息
    count:state.toJS().play.playCount, //播放序号
    playSrc:state.toJS().play.playUrl //播放url
  }
  
}
const mapDispatchToProps = dispatch => {
  return{
    changeBigSize: val => dispatch(playing(val)), //大播放器
    changeSmallSize: val => dispatch(playingsmall(val)), //小播放器
    playUrl:val => dispatch(setUrl(val)),
    playCount:val =>dispatch(setCount(val)),
  }
  
}
export default connect(mapStateToProps,mapDispatchToProps)(Play)

