import styled from 'styled-components'
export const ControlStyle = styled.div`
height:100%;
width:100%;
position:absolute;
top:0;
left:0;
z-index:100;
`
export const PlayStyle = styled.div`
width:100%;
height:100%;
background:url(${props=> props.img})no-repeat center;
// filter: blur(20px);
background-size:cover;
position:relative;
z-index:1;
&:after{
  content: "";
  width:100%;
  height:100%;
  position: absolute;
  left:0;
  top:0;
  background: inherit;
  filter: blur(20px);
  z-index: 2;
}
h2{
  font-size:.14rem;
  line-height:.2rem;
  text-align:center;
  color:#fff;
}
.content{
  width:100%;
  height:100%;
  position:absolute;
  z-index:3;
  padding:0 .2rem;
  .title{
    height:.4rem;
    display:flex;
    align-items:center;
  
    i{
      display:inline-block;
      width:.4rem;
      height:.4rem;
      font-size:.14rem;
      font-style:normal;
      line-height: .4rem;
      text-align: center;
    }
    span{
      display:inline-block;
      width: 70%;
      margin: 0 auto;
      line-height: .4rem;
      text-align: center;
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
      font-size: .18rem;
      color: #fff;
    }

  }
}
.center{
  width:3rem;
  height:3.2rem;
  margin: .4rem auto;
  border-radius:50%;
  img{
    width:100%;
    height:3rem;
    border-radius:50%;
    border:.1rem solid hsla(0,0%,100%,.1)
  }
  p{
    height: .2rem;
    line-height: .2rem;
    font-size: .14rem;
    color: hsla(0,0%,100%,.5);
    text-align:center;
  }
}
.control{
  margin-top:1.2rem;
  display:flex;
  height:.4rem;
  align-items:center;
  font-size:.12rem;
  color:#fff;
  .am-slider-wrapper{
    width:90%;
    .am-slider{
      margin-left:.1rem !important;
      margin-right:.1rem !important;
    }
  }
  .am-slider-step{
    width:87%;
  }
  .am-slider-handle{
    width:.1rem;
    height:.1rem;
    margin-top:-.04rem;
  }
}
.btn{
  display:flex;
  height:.4rem;
  font-size:.3rem;
  align-items:center;
  justify-content:space-between;
  color:pink;
  margin-top:.15rem
}
`