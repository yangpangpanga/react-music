import {fromJS} from 'immutable';
import * as constants from './actionTypes';

const initState = fromJS({
  status:false, //控制是否显示播放界面
  playStyle:false, //控制显示大小播放器
  playList:[],//播放列表
  playCount:null,//当前正在播放的歌曲序号
  playUrl:null,//当前要播放的url
});

export default (state=initState, action) => {
  switch(action.type){
      case constants.PLAYNUM: 
        return state.set('playCount',action.payload)
      case constants.PLAYLIST:
        return state.set('playList',fromJS(action.payload))
      case constants.PLAYSTATUS:
        return state.set ('status',action.payload)
      case constants.PLATYSTYLE:
        return state.set ('playStyle',action.payload)
        case constants.PLATYURL:
          return state.set ('playUrl',action.payload)
      default:
        return state
  }
  
}