import * as constants from './actionTypes';

//控制播放详情页的显示状态
export const playing = (val) => {
  return {
    type: constants.PLAYSTATUS,
    payload:val
  }
}
//控制播放小页的显示状态
export const playingsmall = (val) => {
  return {
    type: constants.PLATYSTYLE,
    payload:val
  }
}
//歌曲列表
export const setPlayList = (val) => {
  // console.log('这是要给reducer的数据啊')
  // console.log(val)
  return {
    type: constants.PLAYLIST,
    payload:val
  }
}
//播放歌曲的序号
export const setCount = (val) => {
  // console.log('这是要给reducer的数据啊')
  // console.log(val)
  return {
    type: constants.PLAYNUM,
    payload:val
  }
}
//播放歌曲的url
export const setUrl = (val) => {
  // console.log('这是要给reducer的数据啊')
  // console.log(val)
  return {
    type: constants.PLATYURL,
    payload:val
  }
}