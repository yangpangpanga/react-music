import React, { Component } from 'react'
import {connect} from 'react-redux'
//导入action
import {playing,playingsmall} from '../play/store/actionsCreator'
//导入样式组件
import {BottomStyle} from './BottomStyle'
 class Bottom extends Component {
   //控制大尺寸显示小尺寸隐藏
   changeSize(val){
     this.props.changeSmallSize(val)
     this.props.changeBigSize(!val)
   }
  render() {
    return (
      <BottomStyle style={this.props.status? {display:'block'}:{display:'none'}} onClick={this.changeSize.bind(this,false)}>
        <img></img>
        <div className="song"></div>
        <div>1</div>
        <div>2</div>
      </BottomStyle>
    )
  }
}
const mapStateToProps = state => {
  return{
    status:state.toJS().play.playStyle //小播放器是否显示
  }
  
}
const mapDispatchToProps = dispatch => {
  return{
    changeBigSize: val => dispatch(playing(val)), //大播放器
    changeSmallSize: val => dispatch(playingsmall(val)) //小播放器
  }
  
}
export default connect(mapStateToProps,mapDispatchToProps)(Bottom)
