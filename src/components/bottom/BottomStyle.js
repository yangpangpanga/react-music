import styled from 'styled-components'
export const BottomStyle = styled.div`
width:100%;
height:.6rem;
display:flex;
align-items:center;
padding:0 .1rem;
justify-content:space-between;
background:#222;
position:fixed;
bottom:0;
left:0;
z-index:999;
img{
  width:.4rem;
  height:.4rem;
  border-radius:50%;
}
div{
  width:.3rem;
  height:.3rem;
}
.song{
  width:2.05rem
}


`