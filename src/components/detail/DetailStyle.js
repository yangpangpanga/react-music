import styled from 'styled-components'

export const DetailStyle = styled.div`
height:100%;
width:100%;
display: flex;
top:0;
flex-direction: column;
position:absolute;
z-index:2;

.top{
  width:100%;
  height:2.62rem;
  background:url(http://p.qpic.cn/music_cover/8HhavvWNIicpyjlv0UnT5TfMaSdbLR6AHptgMOZ6qDykF2bZEkjzuvA/600?n=1) ;
  position:relative
}
.mask{
  width:100%;
  height:2.62rem;
  background:rgba(7,17,27,.4);
  postion:absolute;
  top:0;
  left:0;
  z-index:3;
}
.title{
  font-size:.2rem;
  position:absolute;
  line-height:.4rem;
  top:0;
  margin-left:.3rem
}
.btn{
  height:.32rem;
  width:1.33rem;
  border:1px solid rgb(255, 205, 50);
  text-align:center;
  line-height:.32rem;
  border-radius: 20px;
  position:absolute;
  z-index:4;
  font-size:.1rem;
  bottom:.1rem;
  left:1.2rem
}
.container{
  // flex:1;
  height:61%;
  width:100%;
  overflow: hidden;
  padding: .2rem .3rem;
  font-size:.14rem;
  background:#222;
  ul{
    overflow-y:scroll;
    height:100%;
  }

  li{
    height:.64rem;
    display:flex;
    flex-direction: column;
    justify-content:space-around;

    .first{
      color:#fff;
    }
    .second{
      color:hsla(0,0%,100%,.3)
    }
  }
 
}

`