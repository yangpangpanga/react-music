import React, { Component } from 'react'

import {connect} from 'react-redux'
//导入action
import {playing,playingsmall,setPlayList,setCount,setUrl} from '../play/store/actionsCreator'
// //引入better插件
import BScroll from 'better-scroll'
//导入动画插件
import "animate.css";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
//导入样式组件
import {DetailStyle} from './DetailStyle'
//导入请求
import {getSongUrl} from '../../api/requestw'
// import {getSongsDetail} from '../../api/requesta'
 class Detail extends Component {
  constructor(props){
    super(props)
    this.state={
      sUrl:null,//存放歌曲的url


    }

  }
  componentDidMount() {
    if(this.props.list){
       const wrapper = document.querySelector('.container')
      //选中DOM中定义的 .wrapper 进行初始化
        const scroll = new BScroll(wrapper, {
          // scrollX: false,  //开启横向滚动
          click: true,  // better-scroll 默认会阻止浏览器的原生 click 事件
          // scrollY: true, //关闭竖向滚动
          // pullUpLoad: {
          //   threshold: 50
          // }
        })
    }
    // const wrapper = document.querySelector('.container')
    // //选中DOM中定义的 .wrapper 进行初始化
    // const scroll = new BScroll(wrapper, {
    //   // scrollX: false,  //开启横向滚动
    //   click: true,  // better-scroll 默认会阻止浏览器的原生 click 事件
    //   // scrollY: true, //关闭竖向滚动
    //   // pullUpLoad: {
    //   //   threshold: 50
    //   // }
    // })
  //    // 监控
  //    scroll.on('pullingUp', () => {
  //     console.log('触发上拉刷新啦~~')

  //     let pagenum = this.state.pagenum
  //     let musics = this.state.songslist
  //     let newMusics = musics.concat([
  //         {
  //             title: '测试',
  //             name: '练习'
  //         },
  //         {
  
  //             title: '测试',
  //             name: '练习'
  //         },
  //         {
  
  //             title: '测试',
  //             name: '练习'
  //         },
  //         {
  //             title: '测试',
  //             name: '练习'
  //         }
  //     ])

  //     console.log(`第${pagenum}页`)
  //     console.log('又刷新了')

  //     this.setState({
  //       songslist: newMusics,
  //         pagenum: pagenum+1
   
  //     }, () => {

  //         // 3.切记切记切记：操作完成之后，一定要说一下上拉完毕
  //         scroll.finishPullUp()
  //     })

  // })
  }
  //控制自己组件隐藏
  changeShow(val){
    this.props.showDetail(val)
   
  }
  //控制播放的显示
  willPlay(val,index,id){
    getSongUrl({id})
    .then(res => {
      if(res.code === 200){
        this.props.playUrl(res.data[0].url) //设置播放的url
      } 
    })
    this.props.playCount(index) //设置播放的序号
    this.props.changeBigSize(val)
  }
   //性能优化 
  // shouldComponentUpdate(newProps, newState){
  
  //     return this.props.isShow !== newProps.isShow
  // }

  render() {
    // return <div></div>
    if(this.props.willPlayList){
      console.log(this.props.willPlayList)
      return (
        <ReactCSSTransitionGroup
                      transitionEnter={true}
                      transitionLeave={true}
                      transitionEnterTimeout={1500}
                      transitionLeaveTimeout={1500}
                      transitionName="animated"
                    >
        <DetailStyle style={this.props.isShow ? {display:'block'}:{display:'none'}}
               className={this.props.isShow? 'animated fadeInLeftBig':'animated  fadeOutLeftBig'}>
          <div className="top"  key="amache" onClick={this.changeShow.bind(this,false)} >
            <div className="mask"></div>
            <div className="title"><h1>{this.props.title}</h1></div>
            <div className="btn">随机播放</div>
          </div>
          <div className="container">
            <ul className="containerdetail">
              {this.props.willPlayList.map((item,index) => {
                return(
                  <li key={index} onClick={this.willPlay.bind(this,true,index,item.id)}>
                    <p className="first">{item.name}</p>
                    <p className="second">{item.ar[0].name}</p>
                  </li>
                )
              })}
            </ul>
          </div>
        </DetailStyle>
        </ReactCSSTransitionGroup>
      )
    }else{
      console.log('这个时候还没有数据')
      return <div className="container"></div>
    }
      
 
   
  }
}
const mapStateToProps = state => {
  // console.log('我看这个list是什么数据啊')
  // console.log(state.toJS().play.playList)
  return{
    willPlayList:state.toJS().play.playList
  }
  
}
const mapDispatchToProps = dispatch => {
  return{
    changeBigSize: val => dispatch(playing(val)),
    willPlay:val =>dispatch(setPlayList(val)),
    playCount:val =>dispatch(setCount(val)),
    playUrl:val => dispatch(setUrl(val))
    // changeSmallSize: val => dispatch(playingsmall(val))
  }
  
}
export default connect(mapStateToProps,mapDispatchToProps)(Detail)
