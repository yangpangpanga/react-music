import React, { Component } from 'react'
import Head from '../header/Head'
import TopBar from '../topbar/TopBar'
export default class Top extends Component {
  render() {
    return (
      <div>
        <Head/>
        <TopBar/>

      </div>
    )
  }
}
