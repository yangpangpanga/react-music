const proxy = require('http-proxy-middleware')
const {
  createProxyMiddleware
} = require('http-proxy-middleware');
module.exports = function (app) {
  app.use('/api', createProxyMiddleware({
    target: 'https://c.y.qq.com',
    changeOrigin: true,
    headers: {
      referer: 'https://c.y.qq.com/',
      host: 'c.y.qq.com'
     },
    pathRewrite: {
      "^/api": ""
    },
  }));
  app.use(createProxyMiddleware('/uapi', {
    target: "https://u.y.qq.com",
    //  secure: false,
    changeOrigin: true,
    headers: {
      referer: 'https://i.y.qq.com/',
      host: 'c.y.qq.com'
     },
    pathRewrite: { 
      "^/uapi": " "
    },
  }));
  app.use(createProxyMiddleware('/vapi', {
    target: "http://ustbhuangyi.com",
    //  secure: false,
    changeOrigin: true,
    headers: {
      Referer:' http://ustbhuangyi.com/music/'
      // host: 'c.y.qq.com'
     },
    pathRewrite: { 
      "^/vapi": " "
    },
  }));
};   