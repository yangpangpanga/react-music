// 导入组件
import Recommend from '../pages/recommend/Recommend';
import Singer from '../pages/singer/Singer';
import Rank from '../pages/rank/Rank';
import Search from '../pages/search/Search';

// 导出路由
export default [
 {
     path: '/recommend',
     title: '推荐',
     exact:true,
     component: Recommend,
 },
 {
     path: '/singer',
     title: '歌手',
     component: Singer
 },
 {
     path: '/rank',
     title: '排行',
     component: Rank,
     islogin: true
 },
 {
     path: '/search',
     title: '搜索',
     component: Search
 }
]