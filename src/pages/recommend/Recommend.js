import React, { Component } from 'react'
import {connect} from 'react-redux'
//引入better插件
import BScroll from 'better-scroll'
//导入头部组件
import Carsouel from '../../components/carousel/Carsouel'
import Top from '../../components/top/Top'

//导入歌曲详细界面
import Detail from '../../components/detail/Detail'
//导入样式组件
import {RecommendStyle} from './RecommendStyle'
//导入action
import {setPlayList} from '../../components/play/store/actionsCreator'
import {getSongList,getSongListDetail} from '../../api/requestw'
// //导入vue-musci的接口
// import {getlistApi,getListDetailApi,getSongUrlApi} from '../../api/requestv'
// //导入方法
// import {getPlayCate,getList,getSongList} from '../../api/requesta'
// //导入方法 自己尝试的跨域写法
// import {getData,getRank,getSingerList,getSingerSong,getSearchList} from '../../api/request'
class Recommend extends Component {
  constructor(props){
    super(props)
    this.state={
      playlist:[],
      page:1,//控制首页加载的歌单页数
      detail:false,//控制详情页
      bannerlist:[],//存放轮播图
      someData:[],//不知道是啥接口反正数据挺多的
      recommendList:[],//页面推荐歌单列表
      detailList:{},//详情页要显示的数据
      sType : [],//存歌曲的id
      songUrllist:[]//存获取的歌曲url
    
      // songlist:{
      //   page:1,
      //   categoryId:10000000
      // }

    }
  }
  //控制详情页显示隐藏方法
  showDetail(val,id){

    if(id){
      getSongListDetail({id})
      .then(res => { //增额里面有标题 有图片还没有取
        this.props.setList(res.playlist.tracks)
        // this.setState({
        //   detailList:res.playlist.tracks,
        // },() => {
        //   this.props.setList(this.state.detailList)
        // })
      })
      // getData(id).then(res =>  {
      //   // console.log('自己写的',res)
      //   let sId =[]
      //   let sType = []
      //   res.cdlist[0].songlist.forEach((item) => {
      //     sId.push(item.mid)
      //     sType.push(0)
      //   })
      //   this.setState({
      //     detailList:res.cdlist[0],
      //     sId,
      //     sType
      //   },() => {
      //     this.getDetailMsg()
      //   })
      // })

    }
   
    this.setState({
      detail:val
    })
  }

  componentDidMount() {

    getSongList().then(res => {
      // console.log(res)
      if(res.code === 200){
        this.setState({
          recommendList : res.result
        })
       
      }
    })
  
    const wrapper = document.querySelector('.wrapper')
    //选中DOM中定义的 .wrapper 进行初始化
    const scroll = new BScroll(wrapper, {
      // scrollX: false,  //开启横向滚动
      click: true,  // better-scroll 默认会阻止浏览器的原生 click 事件
      // scrollY: true, //关闭竖向滚动
      pullUpLoad: {
        threshold: 50
      }
    })
     // 监控
     scroll.on('pullingUp', () => {
      console.log('触发上拉刷新啦~~')
      console.log(this.state.page)
      let pagenum = this.state.page + 1
      let musics = this.state.recommendList
      console.log('我看看现在是第几页',pagenum)
      // getList({
      //   page:pagenum,
      //   categoryId:10000000,
      //   limit:20
      // }).then(res => {
      //   if(res.response.code === 0){
      //     let newMusics = res.response.data.list
      //     let list = musics.concat(newMusics)
      //     this.setState({
      //       recommendList: list,
      //       page:pagenum
      //     }, () => {
      //       console.log(this.state.recommendList)
      //         // 3.切记切记切记：操作完成之后，一定要说一下上拉完毕
      //         scroll.finishPullUp()
      //     })
      //   }
      // })


     

  })
  }
  render() {
    console.log(this.state.recommendList)
    return (
      <RecommendStyle>
       
        <Top/>
        <Carsouel/> 
        <h1>热门歌单推荐</h1>
        {/* 歌单列表区域 */}
        <div className="wrapper">
          <ul className="content">
            {this.state.recommendList.map((item,index) => {
              return( <li key={index} onClick={this.showDetail.bind(this,true,item.id)}>
                  <img src={item.picUrl}/>
                  <div>
                    <p>{index} {item.name}</p>
                    {/* <p>{item.copywriter}</p> */}
                  </div>
              </li>
              )
            })} 

          </ul>
        </div>
        
        <Detail isShow={this.state.detail} showDetail={this.showDetail.bind(this)}
         list={this.state.detailList.songlist} title={this.state.detailList.dissname} />
      </RecommendStyle>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return{
    setList : val => dispatch(setPlayList(val))
  }
}

export default connect(() => {return{}},mapDispatchToProps)(Recommend)