import styled from 'styled-components'
export const RecommendStyle = styled.div`
width:100%;
height:100%;
display: flex;
flex-direction: column;
// position:fixed;
h1{
  font-weight:normal;
  font-size:.14rem;
  color:red;
  line-height:.65rem;
  text-align:center

}
.wrapper{
  // height:100%;
  // position: absolute;
  // top:2.38rem;
  overflow:hidden;
  padding:0 .2rem;
  width:100%;
  flex:1;
  top:3.03rem;
  ul{
    width:100%;
  
    li{
      width:100%;
      display:flex;
      height:.62rem;
      align-items:center;
      margin:.05rem 0;
      img{
        width:.6rem;
        height:.6rem;
        margin-right:.1rem
      }
      div{
        font-size:.14rem
      }
    }
  }

}
`