import React, { Component } from 'react'
//引入图片懒加载
import LazyLoad from 'react-lazyload';
//引入better插件
import BScroll from 'better-scroll'
//导入头部组件
import Top from '../../components/top/Top'
import Detail from '../../components/detail/Detail'
//导入样式组件
import {SingerStyle} from './SingerStyle'
//导入接口方法
import{getSingerList} from '../../api/requestw'
// import {getSingers} from '../../api/requesta'
export default class Singer extends Component {
  constructor(props){
    super(props)
    this.state={
      list:[],
      paramsList:['hot','A',"B","C",'D','E','F','G','H',"I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","#"],
      currentIndex:0,
      detail:false,//控制详情页
      scroll:null,
    }
  }
   //控制详情页显示隐藏方法
   showDetail(val){
    console.log('执行显示隐藏')
    this.setState({
      detail:val
    })
  }
//过滤index
changeLetter(letter){
  switch(letter){
    case '1':
      return 'A';
    case '2':
      return 'B';
    case '3':
      return 'C';
      case '4':
      return 'D';
      case '5':
      return 'E';
      case '6':
      return 'F';
      case '7':
      return 'G';
      case '8':
      return 'H';
      case '9':
      return 'I';
      case '10':
      return 'J';
      case '11':
      return 'K';
      case '12':
      return 'L';
      case '13':
      return 'M';
      case '14':
      return 'N';
      case '15':
      return 'O';
      case '16':
      return 'P';
      case '17':
      return 'Q';
      case '18':
      return 'R';
      case '19':
      return 'S';
      case '20':
      return 'T';
      case '21':
      return 'U';
      case '22':
      return 'V';
      case '23':
      return 'W';
      case '24':
      return 'X';
      case '25':
      return 'Y';
      case '26':
      return 'Z';
      default:
        return '#'
  }
}
 compare(property){
  return function(a,b){
      var value1 = a[property];
      var value2 = b[property];
      return value1 - value2;//升序,降序为value2 - value1
  }
}
  componentDidMount() {

    getSingerList('-1').then(res => {
      if(res.code === 200){
        this.setState({
          list:res.artists
        })
      }
    })

    const wrapper = document.querySelector('.singer')
    //选中DOM中定义的 .wrapper 进行初始化
    const scroll = new BScroll(wrapper, {
      probeType: 3,
      click: true, // better-scroll 默认会阻止浏览器的原生 click 事件
    })
    this.setState({
      scroll
    })
    let elTopArr = []
    for (let key in this.refs){
      elTopArr.push(this.refs[key].offsetTop - 88)
    }
    console.log('为什么没有数据呢！！！')
    console.log(this.refs['hot'].offsetTop)
    console.log(this.refs)
    scroll.on('scroll', (pos) => {
      //获取滚动条的距离
      let y = Math.abs(pos.y)
      console.log(y)
      for (let i = elTopArr.length - 1; i >= 0; i--) {
        //判断滚动条的高度大于那个高度，大于他就inx等于对应的inx
        if (y >= elTopArr[i]) {
          this.setState({
            currentIndex: i
          })
          break
        }
      }
    })

  }
  //点击索引跳转具体的界面
  toIndexFn(val){
    if(val === "热门") val = "hot"
    // console.log(val)
    // console.log(this.refs)
    this.state.scroll.scrollToElement(this.refs[val],1000)
  }
  render() {
    return (
      <SingerStyle>
        <Top/>
        <div className="list">
          {this.state.paramsList.map((item,index) => {
            return (
            <p key={index } className={this.state.currentIndex===index? 'active':''} onClick={this.toIndexFn.bind(this,item)}>
              {item}</p>
            )
          })}
        </div>
        <div className="singer" >
          <div>
            {this.state.paramsList.map ((item,index) => {
              return(
                <div  key={index} className="imgset">
                  <h1 ref={item} > {item}</h1>

                  <ul >
                    {this.state.list.map((attr,index) => {
                        return(
                          <li key={index} onClick={this.showDetail.bind(this,true)}>
                            {/* <LazyLoad  
                            key={index}
                             overflow={true}
                            //  height={500}
                             scrollContainer='.imgset'
                            //  scroll={true}
                             offset={10}
                            //  height={'.5rem'}
                            //  placeholder={<img width="100%" height="100%" src={'/logo192.png'} alt="logo"/>}
                            > */}
                            <img src={attr.picUrl} alt=""></img>
                            {/* </LazyLoad> */}
                            <p>{attr.name}</p>
                            
                            </li>
                        )
                    })}
                  </ul>
              </div>
              )
            })}
          </div>
        </div>
        <Detail isShow={this.state.detail} showDetail={this.showDetail.bind(this)}/>
      </SingerStyle>
    )
  }
}
