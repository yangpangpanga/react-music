import styled from 'styled-components'
export const SingerStyle = styled.div `
width:100%;
height:100%;
display: flex;
flex-direction: column;
position:relative;
.list{
  width:.2rem;
  position:absolute;
  right:0;
  top:50%;
  margin-top:-2.15rem;
  p{
    width:.2rem;
    height:.18rem;
    padding:.03rem;
    font-size:.12rem;
  }
  .active{
    color:red;
  }
}
.singer{
  flex:1;
  overflow:hidden;
  font-size:.14rem;
  background:#222;
  padding-left:5%;
  width:95%;
  li{
    height:.6rem;
    display:flex;
    align-items:center;
    color: hsla(0,0%,100%,.5);
    img{
      width:.5rem;
      height:.5rem;
      border-radius:50%;
      margin-right:.2rem;
    }
  }
}
h1{
    font-weight:normal;
    height: .3rem;
    line-height: .3rem;
    font-size: .12rem;
    color: hsla(0,0%,100%,.5);
    background: #333

}
`