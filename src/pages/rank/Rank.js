import React, { Component } from 'react'
import {connect} from 'react-redux'
//导入头部组件
import Top from '../../components/top/Top'
//导入歌曲详细界面
import Detail from '../../components/detail/Detail'
//导入样式组件
import {RankStyle} from './RankStyle'
//导入action
import {setPlayList} from '../../components/play/store/actionsCreator'
//导入接口方法
import {getRank} from '../../api/request'
import {getRanks,getRankDetail,getSongsDetail} from '../../api/requesta'
//导入vue-musci的接口
import {getlistApi,getListDetailApi,getSongUrlApi} from '../../api/requestv'
 class Rank extends Component {
  constructor(props){
    super(props)
    this.state={
      detail:false,//控制详情页
      toplist:[],//排行榜的详情
      songlist:[],
      songType:[], //异步请求参数 type
      songId:[] //异步请求 mid
    }
  }
    //控制详情页显示隐藏方法
    showDetail(val,id){
      //获取排行榜的详情
      getRankDetail(id)
      .then(res => {
        // let imgUrl = res.response.detail.data.data.
        let songId = []
        let songType = []
        const data = res.response.detail.data.data.song
       data.forEach((item) => {
         songId.push([item.singerMid])
         songType.push(0)
       })
       console.log(songId,songType)
        this.setState({
          // detailList:res.cdlist[0],
          songId,
          songType
        },() => {
          console.log([songId])
          // this.getDetailMsg()
          // getSongsDetail(songId).then(res => {
          //   console.log('这又是另外一个接口')
          //   console.log(res)
          // })
        })
      })
      this.setState({
        detail:val
      })
    }
      //获取歌曲详情 判断是否有数据没有就一直发送请求
    getDetailMsg (){    
      console.log('发送异步请求')   
        getSongUrlApi(this.state.songId,this.state.songType).then(res => {
          console.log(res)
          if(res.req_0.data.servercheck){
            this.props.setList({
              songlist:this.state.detailList.songlist,
              urlist:res.req_0.data.midurlinfo
            })
          }
          // else{
          //   this.getDetailMsg()
          // }
        })     
    }
    componentDidMount(){
      getRanks().then(res => {
        console.log(res)

        if(res.response.code === 0){
          this.setState({
            toplist : res.response.data.topList
          })
        }
      })
    }
  render() {

    return (
      <RankStyle>
        <Top/>
          <div className="rank">

            {this.state.toplist.map((item,index) => {
                return(
                  <div className="content" onClick={this.showDetail.bind(this,true,item.id)} key={index}>
                    <img src={item.picUrl} alt="榜单名字"></img>
                    <div className="list">
                      {item.songList.map((attr,index) => {
                        return <p key={index}>{attr.songname } - {attr.singername}</p>
                      })}
                    </div>
                </div>
                )
            })}
            
          </div>
          <Detail isShow={this.state.detail} showDetail={this.showDetail.bind(this)}/>
      </RankStyle>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return{
    setList : val => dispatch(setPlayList(val))
  }
}

export default connect(() => {return{}},mapDispatchToProps)(Rank)
