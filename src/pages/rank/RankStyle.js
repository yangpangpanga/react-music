import styled from 'styled-components'
export const RankStyle = styled.div`
wigth:100%;
height:100%;
display:flex;
flex-direction: column;
.rank{
  flex:1;
  overflow-y:scroll;
  .content{
    display:flex;
    height:1rem;
    padding:.1rem .2rem;
    width:100%;
    align-items:center;
    margin-bottom:.1rem;
    img{
      width:1rem;
      height:1rem;
    }
    .list{
      display:flex;
      flex:1;
      flex-direction: column;
      justify-content:space-around;
      font-size:.12rem;
      margin-left:.2rem;
      p{
        width:2.15rem;
        text-overflow: ellipsis;
        overflow: hidden;
        white-space: nowrap;
        padding: .1rem 0;
      }
  
    }
  }
}
`
//文字颜色
//color:hsla(0,0%,100%,.3);