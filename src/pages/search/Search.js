import React, { Component } from 'react'
//导入antd组件
import { SearchBar } from 'antd-mobile';
//导入异步请求方法
import {getSearchValue,SearchValue} from '../../api/requestw'
//导入头部组件
import Top from '../../components/top/Top'
//导入搜索列表组件
import SearchList from './component/list/SearchList'
//导入搜索结果组件
import Finally from './component/finally/Finally'
//导入样式组件
import {SearchStyle} from './SearchStyle'
export default class Search extends Component {
  constructor(props){
    super(props)
    this.state={
      values:[], //搜索的值
      timer:null, //输入框防抖
      isList:false,
      hotSearchList:[],//放搜索建议列表
      isValue:false //最终每个关键词的搜索结果组件
    }
  }
  //延迟执行搜索
  
  //可以获取输入的值
  onChange (value) {
    console.log('00000')
    clearTimeout(this.state.timer)
    if(value){
        this.setState({
        timer:setTimeout(() => {
          SearchValue({
            keywords:value,
            type:'mobile'
          }).then(res => {
            if(res.code === 200){
              this.setState({ 
                values:res.result.allMatch ,
                isList:true});
            }
            
          })
          this.setState({ value ,isList:true});
        },300)
      })
    }else{
      this.setState({isList:false})
    }
  }
  //控制搜索列表显示隐藏
  changeList(val){
    console.log(123,val)
    if(val === false){
      this.setState({
        value:''
      })
    }
    this.setState({
      isList:val
    })
  }
  //控制最终搜索结果的组件显示
  setValueShow(val){
    this.setState({
      isValue:val
    })
  }
  componentDidMount(){
    //获取热搜关键字
    getSearchValue()
    .then(res => {
      if(res.code === 200){
        this.setState({
          hotSearchList:res.result.hots
        })
      }
    })
  }
  render() {
    return (
      <SearchStyle>
        <Top/>
       <SearchBar placeholder="Search"   onCancel={this.changeList.bind(this,false)} onClear={this.changeList.bind(this,false)}
         onChange={this.onChange.bind(this)} />
       {/* 剩余的搜索部分 */}
       <div className="search">
         {/* 热门搜索 */}
        <div className="searchHot">
          <h1>热门搜索</h1>
          <ul>
            {this.state.hotSearchList.map((item,index) => {
              return(<li key={index}>{item.first}</li>)
            })}
          </ul>
        </div>
        {/* 搜索列表 */}
        <SearchList isList={this.state.isList} valueList={this.state.values} setValue={this.setValueShow.bind(this)}/>
        <Finally isValue={this.state.isValue} ></Finally>
       </div>
      </SearchStyle>
    )
  }
}
