import React, { Component } from 'react'
import {connect} from 'react-redux'
//引入better插件
import BScroll from 'better-scroll'
//导入action
import {setPlayList} from '../../../../components/play/store/actionsCreator'
//导入异步请求方法
import {SearchValue} from '../../../../api/requestw' 
//导入样式组件
import{SearchListStyle} from './SearchlistStyle'
 class SearchList extends Component {
  constructor(props){
    super(props)
    this.state={
      pagenum:1,
    }
  }
  //获取最终的搜索结果
  getSearchValList(val){
    console.log('看看出发没有啊')
    SearchValue({keywords:val})
    .then(res => {
      if(res.code === 200){
        this.props.setList(res.result.songs) //这是要播放的列表
        this.props.setValue(true) //让放最终搜索结果的容器显示
      }
    })
  }
  componentDidMount() {
    const wrapper = document.querySelector('.wrapper')
    //选中DOM中定义的 .wrapper 进行初始化
    const scroll = new BScroll(wrapper, {
      // scrollX: false,  //开启横向滚动
      click: true,  // better-scroll 默认会阻止浏览器的原生 click 事件
      // scrollY: true, //关闭竖向滚动
      pullUpLoad: {
        threshold: 50
      }
    })
     // 监控
     scroll.on('pullingUp', () => {
      console.log('触发上拉刷新啦~~')

      let pagenum = this.state.pagenum
      // let musics = this.state.playlist
      // let newMusics = musics.concat([
         
      // ])

      console.log(`第${pagenum}页`)
      console.log('又刷新了')

      this.setState({
          // playlist: newMusics,
          pagenum: pagenum+1
   
      }, () => {

          // 3.切记切记切记：操作完成之后，一定要说一下上拉完毕
          scroll.finishPullUp()
      })

  })
  }
  render() {
    return (
      <SearchListStyle style={this.props.isList? {display:'block'}:{display:'none'}}>
        <div className="wrapper">
          <ul>
            {this.props.valueList.map((item,index) => {
              return(<li key={index} onClick={this.getSearchValList.bind(this,item.keyword)}>{item.keyword}</li>)
            })}
          </ul>
        </div>
      </SearchListStyle>
    )
  }
}
const mapDispatchToProps = dispatch => {
  return{
    setList : val => dispatch(setPlayList(val))
  }
  
}
export default connect(()=> {return{}},mapDispatchToProps)(SearchList)
