import React, { Component } from 'react'
import {connect} from 'react-redux'
//导入action
// import {setPlayList} from '../../../../components/play/store/actionsCreator'
import {FinallyStyle} from './FinallyStyle'
 class Finally extends Component {
  render() {
    return (
      <FinallyStyle>
         <div className="wrapper" style={this.props.isValue? {display:'block'}:{display:'none'}}>
          <ul>
            {this.props.plyList.map((item,index) => {
              return(<li key={index} onClick={this.getSearchValList.bind(this,item.keyword)}>
                {item.name}  
              </li>)
            })}
          </ul>
        </div>
      </FinallyStyle>
    )
  }
}
const mapStateToProps = state => {
  return{
    plyList:state.toJS().play.playList
  }
}
export default connect(mapStateToProps)(Finally)
