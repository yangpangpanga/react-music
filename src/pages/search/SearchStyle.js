import styled from 'styled-components'
export const SearchStyle = styled.div`
  width:100%;
  height:100%;
  display: flex;
flex-direction: column;
.search{
  flex:1;
  overflow:hidden;

}
.searchHot{
  margin:.1rem;
  background:#222;
  h1{
    margin-bottom: .2rem;
    font-size: .14rem;
    color: hsla(0,0%,100%,.5);
  }
  ul{
    display:felx;
    flex-wrap: wrap;
  }
  li{
    padding:.05rem .1rem;
    font-size: .14rem;
    color: hsla(0,0%,100%,.3);
    border-radius:.06rem;
    background:#333;
    margin-right:.2rem;
    margin-bottom:.1rem;
    // width:33%;

  }
}
.am-search-cancel-show, .am-search-cancel-anim{
  display:none;
}
`