import axios from 'axios'

// 判断当前的运行环境
// isDev 为真 开发环境 --- npm run serve
// isDev 为假 非开发环境（测试环境，生产环境）- npm run build
// const isDev = process.env.NODE_ENV === 'development'

const request = axios.create({
// 根据环境 设置不同的baseURL
// baseURL: '/api'
// baseURL: 'https://u.y.qq.com'

})

// 请求拦截器 - 所有的请求开始之前先到此处
request.interceptors.request.use((config) => {
	// 动画、请求token等
console.log('正在加载....')
return config
}, (error) => {
	return Promise.reject(error)
})

// 响应拦截器 --- 所有请求的相应先到此处
request.interceptors.response.use((response) => {
// 动画隐藏、错误统一处理等
console.log('加载完毕')
return response
}, (error) => {
	return Promise.reject(error)
})


// GET      axios.get(请求路径，{params: 数据对象})   

// export const getData =  (params) => {
//   return request.get('/musics.fcg').then(res => res.data)
// }

//获取一个歌单的歌曲列表disstid 是对应歌单的id
export const getData =  (id) => {
	const playListDetail ={
		'type': 1,
		'json': 1,
		'utf8': 1,
		'onlysong': 0,
		'new_format': 1,
		'disstid': id,
		'g_tk_new_20200303': 1519886966,
		'g_tk': 1519886966,
		// 'loginUin': 1015784200,
		// 'hostUin': 0,
		'format': 'json',
		'inCharset': 'utf8',
		'outCharset': 'utf-8',
		'notice': 0,
		'platform': 'yqq.json',
		'needNewCode': 0
	}
	return request.get("/api/qzone/fcg-bin/fcg_ucc_getcdinfo_byids_cp.fcg",{params:playListDetail})
	.then(res => res.data)
}
//这个接口可以看排行榜 所有的歌单 推荐歌单 轮播图
export const getRank =  (params) => {
	return request.get("/uapi/cgi-bin/musics.fcg?-=recom9581344791033284&g_tk=5381&sign=zza89k82rikv0xypeh1670afea6f1fed9051d1daae90617db9&loginUin=0&hostUin=0&format=json&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq.json&needNewCode=0&data=%7B%22comm%22%3A%7B%22ct%22%3A24%7D%2C%22category%22%3A%7B%22method%22%3A%22get_hot_category%22%2C%22param%22%3A%7B%22qq%22%3A%22%22%7D%2C%22module%22%3A%22music.web_category_svr%22%7D%2C%22recomPlaylist%22%3A%7B%22method%22%3A%22get_hot_recommend%22%2C%22param%22%3A%7B%22async%22%3A1%2C%22cmd%22%3A2%7D%2C%22module%22%3A%22playlist.HotRecommendServer%22%7D%2C%22playlist%22%3A%7B%22method%22%3A%22get_playlist_by_category%22%2C%22param%22%3A%7B%22id%22%3A8%2C%22curPage%22%3A1%2C%22size%22%3A40%2C%22order%22%3A5%2C%22titleid%22%3A8%7D%2C%22module%22%3A%22playlist.PlayListPlazaServer%22%7D%2C%22new_song%22%3A%7B%22module%22%3A%22newsong.NewSongServer%22%2C%22method%22%3A%22get_new_song_info%22%2C%22param%22%3A%7B%22type%22%3A5%7D%7D%2C%22new_album%22%3A%7B%22module%22%3A%22newalbum.NewAlbumServer%22%2C%22method%22%3A%22get_new_album_info%22%2C%22param%22%3A%7B%22area%22%3A1%2C%22sin%22%3A0%2C%22num%22%3A20%7D%7D%2C%22new_album_tag%22%3A%7B%22module%22%3A%22newalbum.NewAlbumServer%22%2C%22method%22%3A%22get_new_album_area%22%2C%22param%22%3A%7B%7D%7D%2C%22toplist%22%3A%7B%22module%22%3A%22musicToplist.ToplistInfoServer%22%2C%22method%22%3A%22GetAll%22%2C%22param%22%3A%7B%7D%7D%2C%22focus%22%3A%7B%22module%22%3A%22QQMusic.MusichallServer%22%2C%22method%22%3A%22GetFocus%22%2C%22param%22%3A%7B%7D%7D%7D")
	.then(res => res.data)
}
//这个接口可以看歌手的索引
const datas={
	'-': 'getUCGI627658406167531',
	g_tk: 5381,
	sign: 'zzajhq772i9raug2f3fef04bda3a286eeb40136f6b80f4a',
	loginUin: 0,
	hostUin: 0,
	format: 'json',
	inCharset: 'utf8',
	outCharset: 'utf-8',
	notice: 0,
	platform: 'yqq.json',
	needNewCode: 0,
	data: {"comm":{"ct":24,"cv":0},"singerList":{"module":"Music.SingerListServer","method":"get_singer_list","param":{"area":-100,"sex":-100,"genre":-100,"index":-100,"sin":0,"cur_page":1}}}
} 
export const getSingerList = () => {
 return request.get('uapi/cgi-bin/musics.fcg',{params:datas})
 .then(res => res.data)
}
//获取某个歌手的热门十首歌
const hotData={
// '	-': 'getSingerSong4491809094481163',
g_tk: 5381,
// sign: 'zzazrq9ra9a0pqdgx8db4e2b21d73a0803bed8de8596b96d436',
loginUin: 0,
hostUin: 0,
format: 'json',
inCharset: 'utf8',
outCharset: 'utf-8',
notice: 0,
platform: 'yqq.json',
needNewCode: 0,
data: {"comm":{"ct":24,"cv":0},"singerSongList":{"method":"GetSingerSongList","param":{"order":1,"singerMid":"001fNHEf1SFEFN","begin":0,"num":10},"module":"musichall.song_list_server"}}
}
export const getSingerSong = () => {
	return request.get('uapi/cgi-bin/musics.fcg',{params:hotData})
	.then (res => res.data)
}
//获取热门搜索列表
const  getSearchData = () =>{
	return{
		'cgiKey': 'GetHomePage',
		'_': new Date().getTime(),
		data: {"comm":{
								"g_tk":1519886966,
								"uin":1015784200,
								"format":"json",
								"inCharset":"utf-8",
								"outCharset":"utf-8",
								"notice":0,
								"platform":"h5"
								,"needNewCode":1},
			"MusicHallHomePage":{
				"module":"music.musicHall.MusicHallPlatform",
				"method":"MobileWebHome",
				"param":{"ShelfId":[101,102,161]}},
				"hotkey":{"module":"tencent_musicsoso_hotkey.HotkeyService",
									"method":"GetHotkeyForQQMusicMobile",
									"param":{"remoteplace":"txt.miniapp.wxada7aab80ba27074","searchid":"1559616839293"}}}
	}
}
export const getSearchList = () => {
	let res = getSearchData()
	return request.get('uapi/cgi-bin/musicu.fcg',{params:res}).then(res => res.data)
}

// # coding=utf-8
// # !/usr/bin/env python
// '''
//     author: dangxusheng
//     desc  : 稍微有点难度，需要多次请求获取key
//     date  : 2018-08-29
// '''

// # 导入模块
// import requests
// from lxml import etree
// import json
// import time

// # 准备全局变量
// headers = {
//     "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.63 Safari/537.36 Qiyu/2.1.1.1",
//     "Referer": "https://y.qq.com/portal/player.html"
// }


// # 获取歌手列表
// # https://y.qq.com/portal/singer_list.html
// def get_singer_list():
//     url = "https://u.y.qq.com/cgi-bin/musicu.fcg?callback=getUCGI25738961582047115&g_tk=5381&jsonpCallback=getUCGI25738961582047115&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&data=%7B%22comm%22%3A%7B%22ct%22%3A24%2C%22cv%22%3A10000%7D%2C%22singerList%22%3A%7B%22module%22%3A%22Music.SingerListServer%22%2C%22method%22%3A%22get_singer_list%22%2C%22param%22%3A%7B%22area%22%3A-100%2C%22sex%22%3A-100%2C%22genre%22%3A-100%2C%22index%22%3A-100%2C%22sin%22%3A0%2C%22cur_page%22%3A1%7D%7D%7D"
//     headers['Referer'] = "https://y.qq.com/portal/singer_list.html"
//     ie = requests.session()
//     rep = ie.get(url, headers=headers)
//     html = rep.content.decode('utf-8')[25:-1]
//     singer_list = json.loads(html)['singerList']['data']['singerlist']
//     ls = []
//     for singer in singer_list:
//         singer_mid = singer['singer_mid']
//         singer_name = singer['singer_name']
//         singer_pic = singer['singer_pic']
//         ls.append({'singer_mid': singer_mid, "singer_name": singer_name, "singer_pic": singer_pic})
//     return ls
//     # print(ls)
//     # exit()


// # 获取专辑列表
// def get_album_list(singer_mid=''):
//     url = "https://u.y.qq.com/cgi-bin/musicu.fcg?callback=getUCGI2613146679247198&g_tk=5381&jsonpCallback=getUCGI2613146679247198&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&data=%7B%22singerAlbum%22%3A%7B%22method%22%3A%22get_singer_album%22%2C%22param%22%3A%7B%22singermid%22%3A%22" + singer_mid + "%22%2C%22order%22%3A%22time%22%2C%22begin%22%3A0%2C%22num%22%3A5%2C%22exstatus%22%3A1%7D%2C%22module%22%3A%22music.web_singer_info_svr%22%7D%7D"
//     headers['Referer'] = "https://y.qq.com/n/yqq/singer/" + singer_mid + ".html"
//     ie = requests.session()
//     rep = ie.get(url, headers=headers)
//     html = rep.content.decode('utf-8')[24:-1]
//     ablum_list = json.loads(html)['singerAlbum']['data']['list']
//     ls = []
//     for item in ablum_list:
//         album_mid = item['album_mid']
//         album_name = item['album_name']
//         singer_mid = item['singer_mid']
//         singer_name = item['singer_name']
//         ls.append(
//             {'album_mid': album_mid, 'album_name': album_name, 'singer_mid': singer_mid, 'singer_name': singer_name})
//     return ls


// # 根据专辑ID下载
// def download_music_by_albumid(albummid='', singername=''):
//     # albummid = "001mTkmb4GJlh4"
//     url = "https://c.y.qq.com/v8/fcg-bin/fcg_v8_album_info_cp.fcg?albummid=" + albummid + "&g_tk=5381&jsonpCallback=albuminfoCallback&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0"
//     headers['Referer'] = "https://y.qq.com/portal/player.html"
//     ie = requests.session()
//     rep = ie.get(url, headers=headers)
//     html = rep.content.decode('utf-8')[19:-1]
//     song_list = json.loads(html)['data']['list']
//     for song in song_list:
//         song_name = song['songname']
//         song_mid = song['songmid']
//         get_key_url = "https://u.y.qq.com/cgi-bin/musicu.fcg?callback=getplaysongvkey0996617262271613&g_tk=5381&jsonpCallback=getplaysongvkey0996617262271613&loginUin=0&hostUin=0&format=jsonp&inCharset=utf8&outCharset=utf-8&notice=0&platform=yqq&needNewCode=0&data=%7B%22req_0%22%3A%7B%22module%22%3A%22vkey.GetVkeyServer%22%2C%22method%22%3A%22CgiGetVkey%22%2C%22param%22%3A%7B%22guid%22%3A%228216405924%22%2C%22songmid%22%3A%5B%22" + song_mid + "%22%5D%2C%22songtype%22%3A%5B0%5D%2C%22uin%22%3A%220%22%2C%22loginflag%22%3A1%2C%22platform%22%3A%2220%22%7D%7D%2C%22comm%22%3A%7B%22uin%22%3A0%2C%22format%22%3A%22json%22%2C%22ct%22%3A20%2C%22cv%22%3A0%7D%7D"
//         rep = ie.get(get_key_url, headers=headers)
//         html = rep.content.decode('utf-8')[32:-1]
//         data = json.loads(html)['req_0']['data']
//         download_url_1 = data['sip'][0]
//         download_url_2 = data['midurlinfo'][0]['purl']
//         filename = data['midurlinfo'][0]['filename']
//         url = download_url_1 + download_url_2
//         rep = ie.get(url, headers=headers, stream=True)
//         with open('./qqmusic/%s' % (song_name + "-" + singername + "-" + filename), 'wb') as file:
//             for byte_data in rep.iter_content(1024):
//                 file.write(byte_data)
//         print('《%s》 下载成功！' % song_name)
//       
