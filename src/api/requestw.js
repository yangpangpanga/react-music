import axios from 'axios'
// import router from '../router'

// 判断当前的运行环境
// isDev 为真 开发环境 --- npm run serve
// isDev 为假 非开发环境（测试环境，生产环境）- npm run build
const isDev = process.env.NODE_ENV === 'development'

const request = axios.create({
// 根据环境 设置不同的baseURL
// baseURL: 'http://localhost:3000/api'
baseURL: isDev ? 'http://localhost:3000/' : 'http://47.93.208.173:3000/'
})

// 请求拦截器 - 所有的请求开始之前先到此处
request.interceptors.request.use((config) => {
	// 动画、请求token等
return config
}, (error) => {
	return Promise.reject(error)
})

// 响应拦截器 --- 所有请求的相应先到此处
request.interceptors.response.use((response) => {
// 动画隐藏、错误统一处理等
return response
}, (error) => {
	return Promise.reject(error)
})
// GET      axios.get(请求路径，{params: 数据对象})   
// export default requeste
//获取搜索内容
export const getSearch = keywords => {
	return request.get('/search',{params:keywords}).then(res=>res.data)
}
//获取轮播图数据
export const getBanner = () => {
	return request.get('/banner?type=2').then(res=>res.data)
}
//获取歌单分类数据
export const getPlatList = () => {
	return request.get('/playlist/catlist').then(res => res.data)
}
//获取歌单分类里面具体的歌单
export const getCatPlayDetail = (params) => {
	return request.get('/top/playlist',{params}).then(res => res.data)
}
//获取歌单数据(用于首页)
export const getSongList = () => {
	return request.get('/personalized?limit=30').then(res=>res.data)
}
//获取精品歌单(另外一个歌单想要获取更多目前没实现)
export const getMoreSongList = (params) => {
	return request.get('/top/playlist/highquality',{params}).then(res=>res.data)
}

//获取歌单详情
export const getSongListDetail = (id) => {
	return request.get('/playlist/detail',{params:id}).then(res=>res.data)
}
//获取歌曲url播放
export const getSongUrl = (id) => {
	return request.get('/song/url',{params:id}).then(res=>res.data)
}
//获取歌曲详情
export const getSongDetail = (id) => {
	return request.get('/song/detail',{params:id}).then(res=>res.data)
}
//获取歌曲歌词
export const getSongLyric = (params) => {
	return request.get('/lyric',{params}).then(res=>res.data)
}
//获取歌手列表信息
export const getSingerList = (int) => {
  return request.get('/artist/list',{params:{type:1,area:7,initial:int}}).then(res => res.data)
}
//获取歌曲评论
export const getSongComment = (params) => {
	return request.get('/comment/music',{params}).then(res=>res.data)
}
// //获取搜索关键字
// export const getSearchHot = () => {
// 	return request.get('/search/hot/detail').then(res => res.data)
// }
//搜索建议
export const SearchValue = (params) => {
	return request.get('/search/suggest',{params}).then(res => res.data)
}
// //搜索多重匹配
// export const searchMulVal = (params) => {
// 	return request.get('/search/multimatch',{params}).then(res => res.data)
// }
//热门搜索
export const getSearchValue = () => {
	return request.get('/search/hot').then(res => res.data)
}
//获取推荐新音乐
 export const getNewMUsic = () => {
	  return request.get('/personalized/newsong').then(res => res.data)
 }
 //榜单内容摘要
 export const getTop = () => {
	 return request.get ('/toplist/detail').then(res=> res.data)
 }
