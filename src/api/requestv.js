import axios from 'axios'
import {
  readSync
} from 'fs'

// 判断当前的运行环境
// isDev 为真 开发环境 --- npm run serve
// isDev 为假 非开发环境（测试环境，生产环境）- npm run build
// const isDev = process.env.NODE_ENV === 'development'

const request = axios.create({
  // 根据环境 设置不同的baseURL
  // baseURL: '/api'
  baseURL: ' /vapi'

})

// 请求拦截器 - 所有的请求开始之前先到此处
request.interceptors.request.use((config) => {
  // 动画、请求token等
  console.log('正在加载....')
  return config
}, (error) => {
  return Promise.reject(error)
})

// 响应拦截器 --- 所有请求的相应先到此处
request.interceptors.response.use((response) => {
  // 动画隐藏、错误统一处理等
  console.log('加载完毕')
  return response
}, (error) => {
  return Promise.reject(error)
})
//获取歌单列表
const listdata = {
  g_tk: 1928093487,
  inCharset: 'utf-8',
  outCharset: 'utf-8',
  notice: 0,
  format: 'json',
  platform: 'yqq',
  hostUin: 0,
  sin: 0,
  ein: 29,
  sortId: 5,
  needNewCode: 0,
  categoryId: 10000000,
  rnd: 0.6230118594036651,
}
export const getlistApi = () => {
  return request.get('/music/api/getDiscList', {
    params: listdata
  }).then(res => res.data)
}
//获取歌单歌曲列表
let params = (id) => {
  return {
    g_tk: 1928093487,
    inCharset: 'utf - 8',
    outCharset: 'utf - 8',
    notice: 0,
    format: 'jsonp',
    disstid: id,
    type: 1,
    json: 1,
    utf8: 1,
    onlysong: 0,
    platform: 'yqq',
    hostUin: 0,
    needNewCode: 0
  }
}
export const getListDetailApi = (id) => {
  const res = params(id)
  return request.get('/', {
    params: res
  }).then(res => res.data)
}
//获取歌曲url
let paramsUrl = (ids, type) => {
  return {
    comm: {
      format: "json",
      g_tk: 5381,
      inCharset: "utf-8",
      needNewCode: 1,
      notice: 0,
      outCharset: "utf-8",
      platform: "h5",
      uin: 0,
    },
    req_0: {
      method: "CgiGetVkey",
      module: "vkey.GetVkeyServer",
      param: {
        guid: 1589121920,
        loginflag: 0,
        platform: "23",
        songmid: ids, //歌单歌曲的id吧
        songtype: type, //存歌曲的类型数组 好像都是0
        uin: "0"
      }
    }
  }
}
export const getSongUrlApi = (id,type) =>{
    const res = paramsUrl(id,type)
    return request.post('/music/api/getPurlUrl',res).then(res => res.data)
}