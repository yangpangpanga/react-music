import axios from 'axios'
import { readSync } from 'fs'

// 判断当前的运行环境
// isDev 为真 开发环境 --- npm run serve
// isDev 为假 非开发环境（测试环境，生产环境）- npm run build
// const isDev = process.env.NODE_ENV === 'development'

const request = axios.create({
// 根据环境 设置不同的baseURL
// baseURL: '/api'
baseURL: ' http://localhost:3200/'

})

// 请求拦截器 - 所有的请求开始之前先到此处
request.interceptors.request.use((config) => {
	// 动画、请求token等
console.log('正在加载....')
return config
}, (error) => {
	return Promise.reject(error)
})

// 响应拦截器 --- 所有请求的相应先到此处
request.interceptors.response.use((response) => {
// 动画隐藏、错误统一处理等
console.log('加载完毕')
return response
}, (error) => {
	return Promise.reject(error)
})

//获取歌单分类
export const getPlayCate = () => {
	return request.get('getSongListCategories').then(res => res.data)
}

//获取歌单的歌曲列表 现在是写固定的热门歌单
export const getList = (params) => {
	return request.get('/getSongLists',{params}).then(res => res.data)
}
//获取歌单列表 这个接口用不了 用的自己配置的跨域
export const getSongList =(params) => {
	return request.get('/getSongListDetail',{params}).then(res =>  res.data)
}
// //获取歌曲详细信息
// export const getSongsDetail = (ids) => {
// 	return request.get('/batchGetSongInfo',{params:{songs:[ids]}}).then(res => res.data)
// }
//获取歌曲详细信息
export const getSongsDetail = (ids) => {
	return request.get('/getSongInfo?songmid=000PJRig3WnHYX').then(res => res.data)
}
//获取排行榜信息
export const getRanks = () => {
	return request.get('getTopLists').then(res => res.data)
}
//获取排行榜详情
export const getRankDetail = (id) => {
	return request.get('/getRanks',{params:{topId:id}}).then(res => res.data)
}
//获取歌手列表
export const getSingers = (params) => {
	return request.get('/getSingerList',{params}).then(res => res.data)
}

